﻿module SomeTry

open System
open System.Collections.Generic
exception NotFound of string

type AtomicTypes = 
        IntAtomicType 
        | StrAtomicType
        | Alpha
        | Beta
    with 
        override x.ToString () =
            match x with
                IntAtomicType ->  "int"
                | StrAtomicType -> "string"
                | Alpha -> "Alpha"
                | Beta -> "Beta"
    

            
        

                
and 
    RangedVariable = 
        RangedVariable of string * Domain

and 
    Domain =   
        ObjectType of ObjectType
        |Kind of Kinds

and 
    Kinds = 
        Star
        |Square

and
    LambdaTermType =
        LTType

and
    TypedVariable = 
        | TypedVariable of string * ObjectType
        member x.Type 
            with get() = 
                (x :> IVariable).Type
        interface IVariable with
            member x.Name = 
                match x with 
                    TypedVariable (name, _ ) -> name
            member x.Type =
                match x with
                    TypedVariable (_, ttype) -> ttype



and
   LambdaTerm = 
        Atom of Atoms
        | Application of LambdaTerm * LambdaTerm
        | LambdaAbstraction of TypedVariable * LambdaTerm
        | Variable of TypedVariable
    with
        member x.Type 
            with get () : ObjectType =
                match x with
                    Application (a, b) -> 
                        match a.Type with
                                FunctionType(t1,t2) -> if t1.CorrespondsTo b.Type then t2 else failwith "ouch"
                                //FunctionType (ObjectType Alpha, ObjectType Beta) -> if b.Type = AtomicType Alpha then AtomicType Beta else failwith "ouch"
                                |_ -> raise (NotFound("not functional"))
                    | LambdaAbstraction (t1, body) -> FunctionType(t1.Type, body.Type)
                    | Atom v -> v.Type
                    | Variable variable -> (variable :> IVariable).Type
    


and
    Atoms =
        A | B | C | IntAtom of int | StringAtom of string | PlusInt | PlusStr
        with
            member x.Type
                with get () : ObjectType =
                    match x with
                        A -> FunctionType (AtomicType Alpha, AtomicType Beta)
                        |C -> FunctionType (AtomicType Alpha, FunctionType(AtomicType Beta, AtomicType Alpha))
                        | B -> AtomicType Alpha
                        | IntAtom _ -> AtomicType IntAtomicType
                        | StringAtom _ -> AtomicType StrAtomicType
                        | PlusStr -> AtomicType StrAtomicType
                        | PlusInt -> AtomicType IntAtomicType

and 
    /// Represents a variable
    IVariable = interface
        abstract Name : string
        abstract Type : ObjectType
    end



                                                                                                                                                         
let solve () = 
    let variable = TypedVariable ("x", AtomicType Alpha)
//    let variable = Variable
   // let var = new LambdaTerm(Variable)
    let variable2 = TypedVariable ("y", AtomicType Beta) 
    let variable3 = TypedVariable ("z", AtomicType Alpha)                                                                                                
    let variable4 = TypedVariable ("zu", RangedVariableType( RangedVariable ("'A", Kind Star)))
    //let Term = LambdaAbstraction (variable, Application (Atom A, Variable variable))
    let  Term2 = Application (Atom A, Variable(variable3)) 
    let k = LambdaAbstraction (variable, (LambdaAbstraction (variable2, Variable(variable))))
    let s = LambdaAbstraction (variable, LambdaAbstraction (variable2, LambdaAbstraction (variable3, Application (Application(Atom C, Variable(variable)), Application (Atom A, Variable(variable3))))))
    Console.WriteLine("_ : {0}", Term2.Type)
    Console.WriteLine(k.Type)
    Console.WriteLine(s.Type)
    Console.ReadKey()    


solve()

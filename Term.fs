﻿module Term

open System
open System.Collections.Generic

exception NotFound


type binder = string ref

type Variable =
    | Variable of string

type Term =
    | Var of Variable
    | App of Term * Term
    | Lambda of Variable * Term
//    with
//        member x.Type 
//            with get () : Term =
//                match x with
//                    App (a, b) -> 
//                        match a.Type with
//                                FunctionType(t1,t2) -> if t1.CorrespondsTo b.Type then t2 else failwith "ouch"
//                                //FunctionType (ObjectType Alpha, ObjectType Beta) -> if b.Type = AtomicType Alpha then AtomicType Beta else failwith "ouch"
//                                |_ -> raise (NotFound("not functional"))
//                    | LambdaAbstraction (t1, body) -> FunctionType(t1.Type, body.Type)
//                    | Atom v -> v.Type
//                    | Variable variable -> (variable :> IVariable).Type

//[<StructuralEquality; StructuralComparison>]
//type ObjectType  = 
//        AtomicType of AtomicTypes
//        |TypeVariable of string
//        |FunctionType of Term * Term
//        |GenericType of RangedVariable*ObjectType
//        member x.CorrespondsTo (b:ObjectType) :bool = 
//            match (x,b) with      
//                (FunctionType(a1,a2),FunctionType(b1,b2)) ->
//                    (a1 = b1 && a2 = b2) 
//                |(AtomicType a, AtomicType b) -> 
//                    (a = b)
//                |(RangedVariableType a, RangedVariableType b) -> 
//                    (a = b)
//                | _ -> false
//
//        override x.ToString () =
//            match x with
//                AtomicType atomicType ->  atomicType.ToString()
//                |FunctionType (oType, oType2) -> oType.ToString()+ "->" + oType2.ToString()
//                | _ -> "nyaa"
//and substitution = (Term * Term) list




//let binder s = ref s


//let destBinder q = !q


//let rec mem x = function
//    | [] -> false
//    | y::ys -> if x = y then true else mem x ys


//let rec isClosedFull trm sb = 
//    match trm with
//    | App(f, a) -> if (isClosedFull f sb) then (isClosedFull a sb) else false
//    | Lambda(q, b) -> isClosedFull b (Bound(q)::sb)
//    | Bound(q) -> mem(Bound(q)) sb
//    | _ -> true 
//
//
//let isClosed trm = isClosedFull trm []

//
//let add x y sb = (x, y)::sb
//
//
//let rec get x = function
//    | [] -> raise NotFound
//    | (t, r)::ls -> if x = t then r else get x ls  

//
//let rec renameFull sb t =
//    match t with
//    | App(l, r) -> App(renameFull sb l, renameFull sb r)
//    | Lambda(q, b) -> 
//        let nq = binder(destBinder q)
//            in let nsb = add(Variable q) (Bound nq) sb
//                in Lambda(nq, renameFull nsb b)
//    | Bound(q) -> try get (Bound q) sb with | :? NotFound -> Bound q


//let rename t = renameFull [] t
//           
//
//let lookup t sb = rename(get t sb)
//

//let rec subst trm sb =
//    let substAux = function
//        | Free s -> Free s
//        | App(l, r) -> App(subst l sb, subst r sb)
//        | Lambda(q, b) -> Lambda(q, subst b sb)
//        | Bound q -> Bound q
//    in try (lookup trm sb) with | :? NotFound -> substAux trm 

let rec substitute term var substitution =
    match term with
        | App(lTerm, rTerm) ->
            App(substitute lTerm var substitution, substitute lTerm var substitution)
        | Lambda(var, lamTerm) ->
            Lambda(var, substitute lamTerm var substitution)
        | Var q ->
            if q.Equals var then
                substitution
            else
                Var q


//let alpha = function 
//    | Lambda(q, b) -> 
//        let nq = binder(destBinder q)
//        in Lambda(nq, subst b (add (Bound q) (Bound dnq) []))
//    | _ -> failwith "Not an alpha-redex" 


//let beta tre = function
//    | App(Lambda(q, b), t) -> subst b (add (Bound q) t [])
//    | _ -> failwith "Not a beta-redex"

//let rec beta term (args : list<Term>) =
//    match term with
//        | App(fst, snd) -> 
//            beta fst (List.append [snd] args)
//        | Lambda(var, term) -> subst b (add (var) t [])
//        | _ -> term

//let eta = function
//    | Lambda(q, App(b, Bound(x))) -> 
//        if q = x then 
//            if(isClosed b) then b else failwith "Not an eta-redex"
//        else failwith "Not an eta-redex"
//    | _ -> failwith "Not an eta-redex"


let alphaEquals_ trm1 trm2 = 
    let rec aeql t1 t2 sb =
        match (t1, t2) with
        | (Free(s1), Free(s2)) -> s1 = s2
        | (Bound b1, Bound b2) -> 
          let qv1 = try (get t1 sb) with | :? NotFound -> t1
              in qv1 = t2
        | (App(f1, a1), App(f2, a2)) -> if aeql f1 f2 sb then aeql a1 a2 sb else false
        | (Lambda(q1, b1), Lambda(q2, b2)) -> aeql b1 b2 (add (Bound q1) (Bound q2) sb)
        | _ -> false
    in aeql trm1 trm2 [] 

let alphaEquals term1 term2 =
    let rec alphaEquals term1 term2 varEqList boundVars =
        match term1 with
            | App(lTerm1, rTerm1) ->
                match term2 with
                    | App(lTerm2, rTerm2) ->
                        alphaEquals lTerm1 lTerm2 varEqList boundVars && 
                        alphaEquals rTerm1 rTerm2 varEqList boundVars
                    | _ -> false
            | Lambda(var1, lamTerm1) ->
                match term2 with
                    | Lambda(var2, lamTerm2) ->
                        if var1 = var2 || List.exists ((=) (var1, var2)) varEqList then // use = or Equals method?
                            alphaEquals lamTerm1 lamTerm2 varEqList (var1 :: var2 :: boundVars)
                        else
                            alphaEquals lamTerm1 lamTerm2 ((var1, var2) :: varEqList) (var1 :: var2 :: boundVars)
                    | _ -> false
            | Var q1 as var1 ->
                match term2 with
                    | Var q2 as var2 ->
                        if var1 = var2 then 
                            true
                        else if // Дописать
                    

//let arrowElimination (term : ObjectType) args

//let newTypeVariable = TypeVariable(DateTime.Now.ToString())
//
//let rec getType (term : Term) (args : list<Term>) (context : list<Term*Term>)   =
//    match term with
//        | App(fst, snd) ->
//            getType fst (List.append [snd] args) context
//        | Lambda(var, term) ->
//            let varType =
//                match (List.find (fun el -> var.Equals(snd el)) context) with
//                    | Some value ->
//                        value
//                    | None ->
//                        failWith("")
//                else
//                    Type(newTypeVariable)
//            let termType = getType(term, [], context) in
//            FunctionType(varType, termType)
//        | Free v -> (variable :> IVariable).Type
//        | Bound v -> (variable :> IVariable).Type